package main

import (
	"testing"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

func TestConfig(t *testing.T) {
	config := Config{
		"http root",
		"web dav root",
	}

	buf, _ := yaml.Marshal(config)
	ioutil.WriteFile(`vpsctl.conf.yaml`, buf, 0666)
}
