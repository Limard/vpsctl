package main

import (
	"log"
	"net/http"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type Config struct {
	HttpRoot string
	WebDAVRoot string
}

func readConfig() *Config {
	var config Config
	buf, e := ioutil.ReadFile("vpsctl.conf.yaml")
	if e != nil {
		createConfig()
	}
	yaml.Unmarshal(buf, &config)

	return &config
}

func createConfig() {
	config := Config{}
	buf, _ := yaml.Marshal(config)
	ioutil.WriteFile(`vpsctl.conf.yaml`, buf, 0666)
}

func main() {
	config := readConfig()

	if len(config.HttpRoot) != 0 {
		http.Handle("/", http.FileServer(http.Dir(config.HttpRoot)))
	}

	if len(config.WebDAVRoot) != 0 {
		http.Handle("/webdav", getWebDAVHandler(config.WebDAVRoot))
	}

	e := http.ListenAndServe(":80", nil)
	if e != nil {
		log.Println(e)
	}
}
