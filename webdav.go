package main

import (
	"log"
	"net/http"
	"net/url"

	"bitbucket.org/Limard/logx"
	"golang.org/x/net/webdav"
	"os"
)

func getWebDAVHandler(davPath string) *webdav.Handler {
	logx.Debugf(`davPath: %s`, davPath)
	webDavHandler := new(webdav.Handler)
	webDavHandler.FileSystem = webdav.Dir(davPath)
	webDavHandler.LockSystem = webdav.NewMemLS()
	webDavHandler.Logger = func(r *http.Request, err error) {
		litmus := r.Header.Get("X-Litmus")
		if len(litmus) > 19 {
			litmus = litmus[:16] + "..."
		}

		switch r.Method {
		case "COPY", "MOVE":
			dst := ""
			if u, err := url.Parse(r.Header.Get("Destination")); err == nil {
				dst = u.Path
			}
			o := r.Header.Get("Overwrite")
			log.Printf("%-20s%-10s%-30s%-30so=%-2s%v", litmus, r.Method, r.URL.Path, dst, o, err)
		default:
			log.Printf("%-20s%-10s%-30s%v", litmus, r.Method, r.URL.Path, err)
		}
	}

	return webDavHandler
}

func RunWebDAVServer(davPath string, davPort string) {
	logx.Debugf(`davPath: %s, davPath: %s`, davPath, davPort)
	os.MkdirAll(davPath+`/Log`, 0666)

	webDavHandler := new(webdav.Handler)
	webDavHandler.FileSystem = webdav.Dir(davPath)
	webDavHandler.LockSystem = webdav.NewMemLS()
	webDavHandler.Logger = func(r *http.Request, err error) {
		litmus := r.Header.Get("X-Litmus")
		if len(litmus) > 19 {
			litmus = litmus[:16] + "..."
		}

		switch r.Method {
		case "COPY", "MOVE":
			dst := ""
			if u, err := url.Parse(r.Header.Get("Destination")); err == nil {
				dst = u.Path
			}
			o := r.Header.Get("Overwrite")
			log.Printf("%-20s%-10s%-30s%-30so=%-2s%v", litmus, r.Method, r.URL.Path, dst, o, err)
		default:
			log.Printf("%-20s%-10s%-30s%v", litmus, r.Method, r.URL.Path, err)
		}
	}

	serverMux := http.NewServeMux()
	serverMux.Handle("/", webDavHandler)

	server := http.Server{
		Addr:    `:` + davPort,
		Handler: serverMux,
	}
	logx.Debugf("Running WebDAV server(:%s)...", davPort)
	err := server.ListenAndServe()
	if err != nil {
		logx.Errorf(`%v`, err)
	}
}
